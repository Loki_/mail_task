import base.BaseTestCase;
import base.ChooseUser;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MailPage;

import java.io.IOException;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginTest extends BaseTestCase {

    @Test
    public void loginTest() throws IOException {

        //EntrySet is a Map and its key and value pair is obtained
        ChooseUser.getUsers().userprops.entrySet().stream().forEach(e -> {
            testUser(e.getKey().toString(), e.getValue().toString());
        });
    }

    private void testUser(String username, String password){
        LoginPage logPass = new LoginPage(getDriver());

        logPass.makeLogin(username, password);
        MailPage inb = new MailPage(getDriver());
        int actualResult = inb.getMailCount();

        Assert.assertNotEquals(actualResult, 0, "The E-mail has no letters");
        inb.mailLogout();
    }
}
