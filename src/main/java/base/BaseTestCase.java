package base;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.LoginPage;
import pages.MailPage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class BaseTestCase extends BaseTest {
    public LoginPage loginPage;
    public MailPage mailPage;
    private ChooseUser loadProps;

    @BeforeTest
    public void setUp() throws IOException {
        chooseBrowsers();
        loadProps = ChooseUser.getUsers();
    }

    @BeforeMethod
    public void getPage() {
        loginPage = new LoginPage(driver);
        PageFactory.initElements(getDriver(), LoginPage.class);
        mailPage = new MailPage(driver);
        PageFactory.initElements(getDriver(), MailPage.class);
        getDriver().manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        getDriver().manage().window().maximize();
    }
}
