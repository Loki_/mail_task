package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginPage extends AbstractPage {
    private String URL = "https://www.mailinator.com";

    // Login field
    @FindBy (id="loginEmail")
    private WebElement userName;

    //Login link
    @FindBy (xpath = "//a[@href='/login.jsp']")
    private WebElement loginLink;

    // Password
    @FindBy (id="loginPassword")
    private WebElement password;

    //Submit
    @FindBy (xpath="//button[contains(@class, 'btn-lg')]")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // Method for get URL for login page
    public LoginPage openUrl() {
        getDriver().get(URL);
        return this;
    }

    // Method for login
    public LoginPage makeLogin(String login, String pass) {
        this.openUrl();
        loginLink.click();
        userName.clear();
        userName.sendKeys(login);
        //System.out.println(login);
        password.clear();
        password.sendKeys(pass);
        //System.out.println(pass);
        loginButton.click();
        return this;
    }

}
